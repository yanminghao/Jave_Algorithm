package com.sirius.algorithm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yanminghao
 */
@SpringBootApplication
public class JaveAlgorithmApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaveAlgorithmApplication.class, args);
	}

}
