package com.sirius.algorithm.test;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author yanminghao
 */
public class TimeComplexity {
    //线性 T(n)=3n 执行线性
    @Test
    public void test01() {

        BigDecimal pi = BigDecimal.valueOf(Math.PI);
        System.out.println(pi);
        System.out.println(Math.PI);
        System.out.println(pi.compareTo(new BigDecimal(Math.PI)));

    }
}
